//
//  SeeReservationsViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore

class SeeReservationsViewController: UIViewController {

    
    //MARK: Outlets
    @IBOutlet weak var textField: UITextView!
    
    @IBAction func showReservationButton(_ sender: Any) {
        
        db.collection("reservation").getDocuments {
            
            (querySnapshot, err) in
            
            self.textField.text = "Look at the terminal for output!"
            
            if (err != nil) {
                print("Error!")
                print(err?.localizedDescription)
            }
            else {
                
                for x in (querySnapshot?.documents)! {
                    // Example 1: output row id
                    print(x.documentID)
                    
                    // Example 2: output all row contents
                    print(x.data())
                    
                    // Example 3: output the name of each person
                    let d = x.data();
                    print(d["username"])
                    print(d["restaurant"])
                    print(d["day"])
                    print(d["numSeat"])
                    
                    
                    
                    print("----")
                    
                }
                
                
            }
            
            
        }
    }
    
    // MARK: Firebase variables
    var db:Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("You are on the see reservations screen")
        
        db = Firestore.firestore()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
